from django.conf import settings

DEFAULTS = {
    'ES_HOSTS': None,
    'ES_KWARGS': {},
    'ES_AUTO_INDEX': True,
    'ES_AUTO_INDEX_IN_CELERY': False,
    'ES_BULK_SEGMENT_SIZE': 1000,
    'ES_FIELD_TYPE_MAPPING': {},
    'ES_BULK_CLASS': None,
}


def get_setting(name):
    return getattr(settings, name, DEFAULTS[name])
