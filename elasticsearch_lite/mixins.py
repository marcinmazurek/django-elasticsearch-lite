import logging

from elasticsearch import Elasticsearch

from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.db.models.fields import NOT_PROVIDED
from django.db.models import get_models, get_app
from django.dispatch import receiver

from .module_settings import get_setting
from .tasks import model_operation


_es_instance = None

logger = logging.getLogger('elasticsearch_lite.mixins')


def get_es_instance(force_new=False):
    global _es_instance
    if _es_instance is None or force_new:
        _es_instance = Elasticsearch(get_setting('ES_HOSTS'),
                                     **get_setting('ES_KWARGS'))
    return _es_instance


class ESIndexedModelMixin(object):
    # It's here so we could easily find models to index without traversing
    # whole class hierarchy
    _ELASTICSEARCH_ENABLED = True

    @classmethod
    def _es_get_option(cls, param, default=None):
        cls_meta = getattr(cls, 'ElasticSearch', None)
        if not cls_meta:
            return default
        return getattr(cls_meta, param, default)

    @classmethod
    def _es_get_fields(cls):
        exclude = cls._es_get_option('exclude', [])
        include = cls._es_get_option('include', [])
        all_fields = cls._meta.fields + cls._meta.many_to_many
        fields = []
        for field in all_fields:
            is_excluded = (field.rel and hasattr(field.rel, 'parent_link') and
                           field.rel.parent_link) or field.name in exclude
            if is_excluded:
                continue

            if include and field.name not in include:
                continue

            fields.append(field)

        return fields

    def es_get_doc(self):
        """
        Create document from basic model fields

        Related fields are not included.
        """
        data = {
            'django_model': self._meta.object_name,
            'django_app': self._meta.app_label
        }
        fields = self.__class__._es_get_fields()
        for field in fields:
            if hasattr(field, 'm2m_reverse_name'):
                try:
                    manager = getattr(self, field.get_attname())
                except ValueError:
                    value = NOT_PROVIDED
                else:
                    value = list(manager.through.objects.values_list(
                        field.m2m_reverse_name(), flat=True
                    ))
            else:
                value = getattr(self, field.get_attname(), field.default)

            if value != NOT_PROVIDED:
                data[field.name] = value

        return data

    def es_meta_data(self, bulk_keys=False):
        "Get elasticsearch document meta data"
        # Class level meta data
        cls = self.__class__
        doc_index = cls._es_get_option('doc_index')
        doc_type = cls._es_get_option('doc_type')
        id_field = cls._es_get_option('id_field')
        retval = {
            'index': doc_index or cls._meta.app_label,
            'doc_type': doc_type or cls._meta.module_name,
        }

        # Instance level meta data
        retval['id'] = getattr(self, id_field or 'pk', None)
        parent_field = cls._es_get_option('parent_field')
        parent = getattr(self, parent_field, None) if parent_field else None
        if parent:
            retval['parent'] = parent

        if bulk_keys:
            # Convert keys for elasticsearch.helpers.bulk
            mapping = {
                'doc_type': '_type',
            }
            retval = dict(
                (mapping.get(k, '_' + k), v) for k, v in retval.items()
            )
        return retval

    def es_index_me(self):
        """
        Add/Update this instance's info in elasticsearch index
        """
        es = get_es_instance()
        doc = self.es_get_doc()
        meta = self.es_meta_data()
        result = es.index(body=doc, **meta)
        return result

    def es_index_me_async(self):
        """
        Add/Update this instance's info in elasticsearch index using celery
        """
        model_operation.delay(
            'es_index_me', self._meta.app_label, self._meta.object_name,
            self.pk
        )

    def es_delete_me(self):
        """
        Delete instance's info from elasticsearch index
        """
        es = get_es_instance()
        meta = self.es_meta_data()
        result = es.delete(**meta)
        return result

    def es_delete_me_async(self):
        """
        Delete instance's info from elasticsearch index using celery
        """
        model_operation.delay(
            'es_delete_me', self._meta.app_label, self._meta.object_name,
            self.pk
        )

    def es_indexed(self):
        "True if object is stored in elasticsearch index"
        es = get_es_instance()
        kwargs = self.es_meta_data()
        return es.exists(**kwargs)

    @classmethod
    def es_get_defined_mappings(cls):
        "Get mappings defined in ElasticSearch class"
        # Default mappings for internal functionality
        properties = {
            'django_model': {
                "type": "string",
                "index": "not_analyzed",
            },
            'django_app': {
                "type": "string",
                "index": "not_analyzed",
            },
        }
        # Add default mappings for field types
        type_map = settings.ES_FIELD_TYPE_MAPPING
        for field in cls._meta.fields:
            field_cls = field.__class__.__name__
            if field_cls in type_map:
                properties[field.name] = type_map[field_cls]

        # Custom Model-level field properties
        custom = cls._es_get_option('properties', {})
        properties.update(custom)

        retval = {
            'properties': properties,
        }

        # Install mapping for parent type if specified
        parent_type = cls._es_get_option('parent_type')
        if parent_type:
            retval['_parent'] = {"type": parent_type}

        # Include document level options (mappings)
        options = cls._es_get_option('mappings')
        if options:
            retval.update(options)

        return retval

    @classmethod
    def es_put_mappings(cls, refresh=True):
        """
        Install mappings defined in cls.Elasticsearch.mappings

        Index have to be created already and given mapping not present.

        If refresh is set to True (default) index is refreshed immediately.
        """
        maps = cls.es_get_defined_mappings()
        if maps:
            meta = cls().es_meta_data()
            body = {
                meta['doc_type']: maps
            }
            es = get_es_instance()
            if not es.indices.exists(index=meta['index']):
                es.indices.create(index=meta['index'])
            es.indices.put_mapping(index=meta['index'],
                                   doc_type=meta['doc_type'], body=body)
            refresh and es.indices.refresh(index=[meta['index']])


@receiver(post_save)
def index_after_save(sender, instance, **kwargs):
    if not getattr(instance, '_ELASTICSEARCH_ENABLED', False):
        return

    default = get_setting('ES_AUTO_INDEX')
    if sender._es_get_option('auto_index', default):
        method = instance.es_index_me
        if get_setting('ES_AUTO_INDEX_IN_CELERY'):
            method = instance.es_index_me_async

        try:
            method()
        except Exception:
            logger.exception('Unable to schedule indexing for %s %s',
                             instance.__class__, instance.pk)


@receiver(post_delete)
def remove_after_delete(sender, instance, **kwargs):
    if not getattr(instance, '_ELASTICSEARCH_ENABLED', False):
        return

    default = get_setting('ES_AUTO_INDEX')
    if sender._es_get_option('auto_index', default):
        method = instance.es_delete_me
        if get_setting('ES_AUTO_INDEX_IN_CELERY'):
            method = instance.es_delete_me_async

        try:
            method()
        except Exception:
            logger.exception('Unable to schedule indexing for %s %s',
                             instance.__class__, instance.pk)


def get_registered(app_label=None):
    """
    Return all model classes registered for indexing.

    if app_label is specified only models from given app are returned.
    """
    all_models = [m for m in get_models()
                  if getattr(m, '_ELASTICSEARCH_ENABLED', False)]
    if app_label is not None:
        app = get_app(app_label)
        app_models = get_models(app)
        all_models = [m for m in all_models if m in app_models]

    return all_models
