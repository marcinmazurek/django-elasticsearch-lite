from elasticsearch import Elasticsearch, NotFoundError

from django.test import TestCase, SimpleTestCase, TransactionTestCase

from testapp.models import ExampleModel, ExampleModelChild, SingleFieldModel, \
        ModelWithFkey
from elasticsearch_lite import bulk_index, auto_index, mixins, \
        bulk_index_async, bulk


class TestSimpleIndex(TestCase):

    def setUp(self):
        self.es = Elasticsearch()

    def tearDown(self):
        try:
            self.es.indices.delete(index=['testapp'])
        except Exception:
            pass

    def is_in_index(self, pk, doc_type='examplemodel'):
        return self.es.exists(index='testapp', doc_type=doc_type, id=pk)

    def get_data(self, pk):
        return self.es.get(index='testapp', doc_type='examplemodel', id=pk)

    def test_single_object_indexed(self):
        instance = ExampleModel.objects.create(name='SampleName', count=23)
        instance.es_index_me()
        self.assertTrue(self.is_in_index(instance.pk))
        self.assertEquals(
            self.is_in_index(instance.pk),
            instance.es_indexed()
        )

    def test_single_object_indexed_async(self):
        instance = ExampleModel.objects.create(name='SampleAsync', count=23)
        instance.es_index_me_async()
        self.assertTrue(self.is_in_index(instance.pk))

    def test_delete(self):
        instance = ExampleModel.objects.create(name='ToDelete', count=23)
        instance.es_index_me()
        self.assertTrue(self.is_in_index(instance.pk))
        instance.es_delete_me()
        self.assertFalse(instance.es_indexed(), 'Present after delete')

    def test_delete_async(self):
        instance = ExampleModel.objects.create(name='ToDeleteAsync', count=23)
        instance.es_index_me()
        self.assertTrue(self.is_in_index(instance.pk))
        instance.es_delete_me_async()
        self.assertFalse(instance.es_indexed(), 'Present after delete')

    def test_bulk_indexing(self):
        instances = {}
        for x in xrange(10):
            instance = ExampleModel.objects.create(name='SampleName' + str(x),
                                                   count=23 + x)
            instances[instance.pk] = instance

        bulk_index(ExampleModel.objects.all())
        for pk, instance in instances.items():
            result = self.get_data(instance.pk)
            source = result['_source']
            self.assertEquals(source['count'], instance.count)
            self.assertEquals(source['name'], instance.name)

    def test_mappings_installation(self):
        get_map_kwargs = dict(
            index=['testapp'], doc_type='examplemodel', field='count'
        )
        self.es.indices.create(index=['testapp'])
        with self.assertRaises(NotFoundError):
            self.es.indices.get_field_mapping(**get_map_kwargs)

        ExampleModel.es_put_mappings(refresh=True)
        result = self.es.indices.get_field_mapping(**get_map_kwargs)
        fmap = result['testapp']['mappings']['examplemodel']
        self.assertEquals(fmap['count']['mapping']['count']['type'], 'string')

    def test_automatic_indexing(self):
        instance = ExampleModel(name="Name", count=1)
        # disabled
        instance.save()
        self.assertFalse(self.is_in_index(instance.pk))
        with self.assertRaises(NotFoundError):
            self.es.indices.refresh(index=['testapp'])

        # enabled
        instance = ExampleModelChild(name="Name", count=1)
        instance.save()
        self.es.indices.refresh(index=['testapp'])
        self.assertTrue(self.is_in_index(instance.pk, 'examplemodelchild'))

        # enabled on delete
        pk = instance.pk
        instance.delete()
        self.assertFalse(self.is_in_index(pk), 'Present after delete')

    def test_child_with_parent_indexed(self):
        ExampleModel.es_put_mappings()
        ExampleModelChild.es_put_mappings()
        child = ExampleModelChild.objects.create(name="Child", count=1,
                                                 parent='11234556')
        self.assertTrue(child.es_indexed())

    def test_foreign_key_collected_properly(self):
        parent = SingleFieldModel.objects.create(name="Model1", count=1,
                                                 included=1)
        child = ModelWithFkey(fkey=parent, name='Child')
        doc = child.es_get_doc()
        self.assertEquals(doc['fkey'], parent.id)

    def test_m2m_field_collected_properly(self):
        ex1 = ExampleModel.objects.create(name="Model1", count=1)
        ex2 = ExampleModel.objects.create(name="Model2", count=2)
        parent = SingleFieldModel.objects.create(name="Model1", count=1,
                                                 included=1)
        child = ModelWithFkey(name='Child', fkey=parent)
        child.save()
        child.m2m = [ex1, ex2]
        doc = child.es_get_doc()
        assert 'm2m' in doc
        self.assertEquals(doc['m2m'], [ex1.id, ex2.id])


class TestAsyncBulkIndexing(TransactionTestCase):

    def setUp(self):
        self.es = Elasticsearch()
        self.es.indices.delete(index=['testapp'], ignore=404)
        self.es.indices.create(index=['testapp'])
        self.instances = {}
        for x in xrange(100):
            instance = ExampleModel.objects.create(name='SampleName' + str(x),
                                                   count=23 + x)
            self.instances[instance.pk] = instance

    def tearDown(self):
        self.es.indices.delete(index=['testapp'], ignore=404)

    def check_instancs_indexed(self):
        self.es.indices.refresh(index=['testapp'])
        for pk, instance in self.instances.items():
            result = self.get_data(instance.pk)
            source = result['_source']
            self.assertEquals(source['count'], instance.count)
            self.assertEquals(source['name'], instance.name)

    def get_data(self, pk):
        return self.es.get(index='testapp', doc_type='examplemodel', id=pk)

    def test_async_bulk_indexing(self):
        bulk_index_async(ExampleModel.objects.all(), step_size=10)
        self.check_instancs_indexed()

    def test_async_bulk_indexing_speedup(self):
        bulk_index_async(ExampleModel.objects.all(), step_size=10,
                         klass=bulk.SpeedupBulkIndexer)
        self.check_instancs_indexed()


class TestMixinOptions(SimpleTestCase):

    def test_field_excluded(self):
        fields = ExampleModelChild._es_get_fields()
        field_names = [f.name for f in fields]
        self.assertFalse('count' in field_names)
        self.assertTrue('child_field' in field_names)

    def test_field_included(self):
        fields = SingleFieldModel._es_get_fields()
        field_names = [f.name for f in fields]
        self.assertFalse('count' in field_names)
        self.assertTrue('included' in field_names)

    def test_foreign_key_included(self):
        fields = ModelWithFkey._es_get_fields()
        field_names = [f.name for f in fields]
        #import ipdb;ipdb.set_trace()
        self.assertTrue('fkey' in field_names)
        self.assertTrue('name' in field_names)

    def test_parent_link_field_excluded(self):
        fields = ExampleModelChild._es_get_fields()
        field_names = [f.name for f in fields]
        self.assertFalse('examplemodel_ptr' in field_names)

    def test_auto_index_manager(self):
        instance = ExampleModel(name="Name", count=1)

        before_value = instance.ElasticSearch.auto_index
        value_to_set = not before_value
        with auto_index(instance, value_to_set):
            managed_value = instance.ElasticSearch.auto_index
        after_value = instance.ElasticSearch.auto_index

        self.assertEquals(managed_value, value_to_set)
        self.assertEquals(after_value, before_value)

    def test_hosts_setting(self):
        hosts = [{'host': 'some_host', 'port': '9200'}]
        with self.settings(ES_HOSTS=hosts):
            es = mixins.get_es_instance(force_new=True)
            self.assertEquals(es.transport.hosts, hosts)

    def test_kwargs_setting(self):
        class Cls(object):
            pass
        kwargs = {'connection_class': Cls}
        with self.settings(ES_KWARGS=kwargs):
            es = mixins.get_es_instance(force_new=True)
            self.assertEquals(es.transport.connection_class, Cls)

    def test_registered_models_found(self):
        # all models
        models = mixins.get_registered()
        names = [model.__name__ for model in models]
        self.assertTrue('ExampleModel' in names, '1st level child')
        self.assertTrue('ExampleModelChild' in names, '2nd level child')
        self.assertFalse('SomeOtherModel' in names, 'Not registered model')
        self.assertTrue('ExampleModelInAnotherAPP' in names, 'testapp2 model')

        # only single app
        app_models = mixins.get_registered('testapp2')
        app_names = [model.__name__ for model in app_models]
        self.assertFalse('ExampleModel' in app_names, '1st level child')
        self.assertTrue('ExampleModelInAnotherAPP' in app_names,
                        'testapp2 as only model')

    def test_custom_fields_in_document(self):
        instance = ExampleModel(name="Name", count=1)
        doc = instance.es_get_doc()
        self.assertTrue('other_name' in doc)

    def test_custom_index(self):
        instance = SingleFieldModel(name='XXX', included=1)
        meta = instance.es_meta_data()
        self.assertEquals(meta['index'], 'other_index')
        self.assertEquals(meta['doc_type'], 'other_type')

    def test_custom_id_and_parent(self):
        instance = SingleFieldModel(name='XXX', parent='YYY', included=1)
        meta = instance.es_meta_data()
        self.assertEquals(meta['id'], 'XXX')
        self.assertEquals(meta['parent'], 'YYY')

    def test_default_mappings(self):
        maps = SingleFieldModel.es_get_defined_mappings()
        maps['_parent']['type'] == 'parent_class'
        properties = maps['properties']
        properties['django_model']['index'] == 'not_analyzed'
        properties['django_app']['index'] == 'not_analyzed'

    def test_model_field_mappings(self):
        maps = SingleFieldModel.es_get_defined_mappings()
        properties = maps['properties']
        for field in SingleFieldModel._meta.fields:
            if field.__class__.__name__ == 'CharField':
                self.assertTrue(field.name in properties)
                self.assertEquals(properties[field.name]['index'],
                                  'not_analyzed')
